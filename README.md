<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your artifact name -->
# RBAC Migration

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview
This Pre-Built Automation allows users to export and import custom IAP groups and their associated roles (NOT associated groups)  between IAP environments. 

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->

<table><tr><td>
  <img src="./images/workflow.png" alt="workflow" width="800px">
</td></tr></table>

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 5 minutes

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`
  * IAP Admin user access
  * App-Artifacts Ver: `^6.0.1-2021.1.0`

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

The main benefits and features of the Pre-Built are outlined below.

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
 * Allows the migration of custom groups between environments without direct Mongo access
 * Promote groups with their full list of roles

## Future Enhancements
 * Add support for group - group mapping
<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>

<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:
Navigate to RBAC Migration operations manager entry, make sure you are an IAP admin, click start manually, and follow directions in active job view. 
In order to export group(s), choose export in the next form, pick group(s) to export:

<table><tr><td>
  <img src="./images/formExport.png" alt="export" width="600px">
</td></tr></table>

Copy the generated content to clipboard:

<table><tr><td>
  <img src="./images/exportContent.png" alt="exportContent" width="600px">
</td></tr></table>


In order to import, pick import in the next form, then paste the content (previously copied) into another IAP environment. 

<table><tr><td>
  <img src="./images/formImport.png" alt="import" width="600px">
</td></tr></table>

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
