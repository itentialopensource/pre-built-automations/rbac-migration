
## 0.0.6 [05-25-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/rbac-migration!8

---

## 0.0.5 [07-18-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/rbac-migration!7

---

## 0.0.4 [02-14-2022]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/rbac-migration!6

---

## 0.0.3 [07-02-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/rbac-migration!4

---

## 0.0.2 [01-07-2021]

* Patch/lb 515 master

See merge request itentialopensource/pre-built-automations/rbac-migration!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n
